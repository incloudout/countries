use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Empty the countries table
        DB::table(config('countries.table_name'))->delete();

        //Get all of the countries
        $countries = Countries::getList();
        foreach ($countries as $countryId => $country){
            DB::table(config('countries.table_name'))->insert([
                'id' => $countryId,
                'capital' => $country['capital'] ?? null,
                'citizenship' => $country['citizenship']  ?? null,
                'country_code' => $country['country-code'],
                'currency' => $country['currency']  ?? null,
                'currency_code' => $country['currency_code']  ?? null,
                'currency_sub_unit' => $country['currency_sub_unit']  ?? null,
                'full_name' => $country['full_name']  ?? null,
                'iso_3166_2' => $country['iso_3166_2'],
                'iso_3166_3' => $country['iso_3166_3'],
                'name' => $country['name'],
                'name_en' => $country['name_en'],
                'name_pt' => $country['name_pt'],
                'region_code' => $country['region-code'],
                'sub_region_code' => $country['sub-region-code'],
                'eea' => (bool)$country['eea'],
                'calling_code' => $country['calling_code'],
                'currency_symbol' => $country['currency_symbol']  ?? null,
                'flag' => $country['flag']  ?? null,
                'language' => $country['language']  ?? null,
            ]);
        }
    }
}
