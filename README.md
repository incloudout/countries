# Laravel Countries

[![Latest Stable Version](https://poser.pugx.org/incloudout/countries/v/stable)](https://packagist.org/packages/incloudout/countries)
[![Total Downloads](https://poser.pugx.org/incloudout/countries/downloads)](https://packagist.org/packages/incloudout/countries)
[![Latest Unstable Version](https://poser.pugx.org/incloudout/countries/v/unstable)](https://packagist.org/packages/incloudout/countries)
[![License](https://poser.pugx.org/incloudout/countries/license)](https://packagist.org/packages/incloudout/countries)

Laravel Countries is a bundle for Laravel, providing Almost ISO 3166_2, 3166_3, currency, Capital and more for all countries.

**Please note that this package is for Laravel 5 only**

## Installation

Add `incloudout/countries` to `composer.json`.

    composer require incloudout/countries
    
Run `composer update` to pull down the latest version of Country List.

## Model

You can start by publishing the configuration. This is an optional step, it contains the table name and does not need to be altered. If the default name `countries` suits you, leave it. Otherwise run the following command

    $ php artisan vendor:publish

Next generate the migration file:

    $ php artisan countries:migration
    
It will generate the `<timestamp>_setup_countries_table.php` migration and the `CountriesSeeder.php` seeder. To make sure the data is seeded insert the following code in the `seeds/DatabaseSeeder.php`

    //Seed the countries
    $this->call('CountriesSeeder');
    $this->command->info('Seeded the countries!'); 

You may now run it with the artisan migrate command:

    $ php artisan migrate --seed
    
After running this command the filled countries table will be available